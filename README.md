# HttpClient 学习

* 本项目, 主要用于快速打通 api 接口使用

# 使用方法

1. 在编辑器（eclipse/idea）中导入此项目

2. 将`src/lib`中的包, 添加到项目依赖, 整体项目不报错即可

3. 在`Test.java`中, 运行 main 方法。在控制台中, 即可看到请求与相应

# 请求效果

![输入图片说明](src/img/image.png)

# 已经实现的功能

- [ ] post 请求

    - [ ] post 请求1
    - [ ] post 请求2
    - [ ] post 请求3
    - [ ] post 请求4

- [x] get 请求

