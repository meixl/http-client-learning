import java.util.HashMap;

public class Test {

    public static void main(String[] args) {
        testWeather();
    }

    /**
     * 测试高德天气
     *
     * 地址：https://lbs.amap.com/api/webservice/guide/api/weatherinfo
     */
    private static void testWeather() {
        String url = "https://restapi.amap.com/v3/weather/weatherInfo";

        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("key", "44b0e32b2359334a1a306c440a3fb74b");
        paramMap.put("city", "330402");

        HttpClientUtil.doGet(url, null, paramMap);
    }

}
